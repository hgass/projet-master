import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;

public class MajoriteAbsolue {

	public static int checkNbVote(int x, int[] Vote, int i, int j) {
		int countX = 0;
		
		if (i > j) {
			System.out.println("Mauvais i et j");
			return -1;		
		}
		if (i < 0) {
			System.out.println("Mauvais i");
			return -1;		
		}
		if (j > Vote.length) {
			System.out.println("Mauvais j");
			return -1;
		}
		for (int z = i; z<=j; z++) {
			if (Vote[z] == x) {
				countX++;
			}
		}
		return countX;
	}

	public static int Majorite_Absolue(int[] Vote) {
		int value;
		int maj = (Vote.length / 2) + 1;
		for (int i = 0; i>Vote.length/2; i++) {
			value = checkNbVote(Vote[i], Vote, 0, Vote.length-1);
			if (value >= maj) { 
				return Vote[i];
			}
		}
		return -1;
	}	

	public static void main (String[] args) {

		int Vote[] = {1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 1, 2, 2, 2, 2, 3, 4, 5, 5, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 5, 4, 5, 6, 6};

		int myMaj = Majorite_Absolue(Vote);
		System.out.print("Check si MAJORITE-ABSOLUE pour un candidat : ");
		if (myMaj == -1) {
			System.out.println("Personne n'a la majorite absolue");
		} else {
			System.out.println(myMaj);
		}	
	}
} 		
