import java.util.Map;
import java.util.HashMap;

public class MajoriteAbsolueDiv {

    	public static class Tuple<Left, Right> {
      		protected Left left;
      		protected Right right;
		public Tuple() {
		}
      		public Tuple(Left l, Right r) {
        		this.left = l;
			this.right = r;
      		}
      		public Left getLeft() {return this.left;}
      		public Right getRight() {return this.right;}
    	}

	public static int checkNbVote(int x, int[] Vote, int i, int j) {
		int countX = 0;
		
		if (i > j) {
			System.out.println("Mauvais i et j");
			return -1;		
		}
		if (i < 0) {
			System.out.println("Mauvais i");
			return -1;		
		}
		if (j > Vote.length) {
			System.out.println("Mauvais j");
			return -1;
		}
		for (int z = i; z<=j; z++) {
			if (Vote[z] == x) {
				countX++;
			}
		}
		return countX;
	}

	public static Tuple<Boolean, Integer> Majorite_Absolue_Div(int[] Vote) {
		return MajoriteAbsolueDivProcess(Vote, 0, Vote.length-1);
	}

	public static Tuple<Boolean, Integer> MajoriteAbsolueDivProcess(int[] Vote, int i, int j) {
		if (i >= j) { return new Tuple<Boolean, Integer>(true, Vote[i]); }

		Tuple<Boolean, Integer> L = MajoriteAbsolueDivProcess(Vote, i, (i+j-1)/2);
		Tuple<Boolean, Integer> R = MajoriteAbsolueDivProcess(Vote, (i+j+1)/2, j);

		if (!L.getLeft() && !R.getLeft()) {
			return new Tuple<Boolean, Integer>(false, 0);
		}
		else {
			if (L.getRight() == R.getRight()) {
					return new Tuple<Boolean, Integer>(true, L.getRight());
			}
			else if (checkNbVote(L.getRight(), Vote, i, j) > (j-i+1)/2) {
					return new Tuple<Boolean, Integer>(true, L.getRight());
			}
			else if (checkNbVote(R.getRight(), Vote, i, j) > (j-i+1)/2) {
				return new Tuple<Boolean, Integer>(true, R.getRight());
			}
		}
		return new Tuple<Boolean, Integer>(false, 0);
	}
	
	public static void main (String[] args) {

		int Vote[] = {1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 1, 2, 2, 2, 2, 3, 4, 5, 5, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 5, 4, 5, 6, 6};

		Tuple<Boolean, Integer> myMaj = Majorite_Absolue_Div(Vote);
		System.out.print("Check si MAJORITE-ABSOLUE-DIV pour un candidat : ");
		if (myMaj.getLeft()) {
			System.out.println(myMaj.getRight());
		} else {
			System.out.println("Personne n'a la majorite absolue");
		}
	}
} 
