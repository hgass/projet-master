import java.util.Scanner;

public class Election {

	public static int checkNbVote(int i, int j, int x, int[] Vote) {
		int countX = 0;
		
		if (i > j) {
			System.out.println("Mauvais i et j");
			return -1;		
		}
		if (i < 0) {
			System.out.println("Mauvais i");
			return -1;		
		}
		if (j > Vote.length-1) {
			System.out.println("Mauvais j");
			return -1;
		}
		for (int z = i; z<=j; z++) {
			if (Vote[z] == x) {
				countX++;
			}
		}
		return countX;
	}

	public static void main (String[] args) {

		int Vote[] = {1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 1, 2, 2, 2, 2, 3, 4, 5, 5, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 5, 4, 5, 6, 6};
		
		Scanner reader = new Scanner (System.in);
		System.out.println("Recherche du nombre de vote pour le candidat x : ");
		System.out.print("- Entrer la valeur base du tableau i : ");
		int i = reader.nextInt();
		System.out.print("- Entrer la valeur haute du tableau j : ");
		int j = reader.nextInt();
		System.out.print("- Entrer le numero du candidat x : ");
		int x = reader.nextInt();

		int monCount = checkNbVote(i, j, x, Vote);
		if (monCount >= 0) {
			System.out.println("- Le nombre de vote pour ce candidat est de : " + monCount);
		}
	}
} 		
