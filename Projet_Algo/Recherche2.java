import java.util.Scanner;

public class Recherche2 {
	
	private int[] A;

	public Recherche2(int[] z) {
		this.A = z;
	}

	public boolean Xsearch (int findX) {

		int q = 0;
		while (q < this.A.length) {
			if (this.A[q] == findX) {
				return true;
			}
			else {
				q = q +1;
			}
		}
		return false;
	}

	public static void main (String[] args) {
		
		int A[] = {2, 9, 5, 10, 4, 7, 33, 25, 26, 15};
		
		Scanner reader = new Scanner (System.in);
		System.out.print("- Entrer la valeur rechercher : ");
		int findX = reader.nextInt();

		Recherche2 maRecherche = new Recherche2(A);
		
		boolean X = maRecherche.Xsearch(findX);
		System.out.println(X);

	}
}
