import java.util.Scanner;

public class Recherche {
	
	public static boolean Xsearch (int findX, int A[]) {
		return Xsearch_process(findX, A, 0);
	}

	public static boolean Xsearch_process (int findX, int A[], int i) {
		if (i >= A.length) return false;
		if (A[i] == findX) return true;
		return Xsearch_process(findX, A, i+1);
	}

	public static void main (String[] args) {
		
		int A[] = {2, 9, 5, 10, 4, 7, 33, 25, 26, 15};
		
		Scanner reader = new Scanner (System.in);
		System.out.print("- Entrer la valeur rechercher : ");
		int findX = reader.nextInt();

		boolean X = Xsearch(findX, A);
		System.out.println(X);
	}
}
