import java.util.Scanner;
import java.util.ArrayList;

public class Intersection {
	
	public static ArrayList<Integer> Inter(int[] A, int[] B) {
		return Inter_process(A, B, 0);
	}

	public static ArrayList<Integer> Inter_process (int[] A, int[] B, int ib) {
		ArrayList<Integer> tmp = new ArrayList<Integer>();

		if (ib < B.length) {
			tmp.addAll(Inter_process(A, B, ib+1));
			if (Xsearch(B[ib], A) != false && tmp.contains(B[ib]) == false) {
				tmp.add(B[ib]);
			}
		}
		return tmp;
	}

	public static boolean Xsearch (int findX, int A[]) {
		return Xsearch_process(findX, A, 0);
	}

	public static boolean Xsearch_process (int findX, int A[], int i) {
		if (A[i] == findX) return true;	
		if (i+1 >= A.length) return false;
		return Xsearch_process(findX, A, i+1);
	}

	public static void main (String[] args) {
		
		int A[] = {2, 9, 5, 10, 4, 7, 33, 25, 26, 15};
		int B[] = {3, 41, 2, 26, 33, 25, 26, 15, 200};

		ArrayList<Integer> res = Inter(A, B);
		System.out.println("- les valeurs contenues dans A ET dans B sont : ");
		System.out.println(res);
	}
}
