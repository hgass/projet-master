import java.util.Scanner;
import java.util.ArrayList;

public class DifferenceTri {
	
	public static ArrayList<Integer> DiffTri (int[] A, int[] B) {
		return DiffTri_process(A, B, 0, 0);
	}

	public static ArrayList<Integer> DiffTri_process (int[] A, int[] B, int ia, int ib) {
		ArrayList<Integer> tmp = new ArrayList<Integer>();
		
		if (ia < A.length) {
			if (XsearchTri(A[ia], B) == false) {
				tmp.add(A[ia]);
			}
			tmp.addAll(DiffTri_process(A, B, ia+1, ib));
			return tmp;
		}

		if (ib < B.length) {
			if (XsearchTri(B[ib], A) == false) {
				tmp.add(B[ib]);
			}
			tmp.addAll(DiffTri_process(A, B, ia, ib+1));
			if (ib != 0) { return tmp; }
		}
		return tmp;
	}

	public static boolean XsearchTri (int findX, int A[]) {
		return XsearchTri_process(findX, A, 0, A.length-1);
	}

	public static boolean XsearchTri_process (int findX, int A[], int i, int j) {
		int k = (i + j) /2;		
		if (findX == A[k]) return true;
		if (i >= j) return false;
		if (findX < A[k]) {
			return XsearchTri_process(findX, A, i, k-1);
		}
		else {
			return XsearchTri_process(findX, A, k+1, j);
		}
	}

	public static void main (String[] args) {
		
		int A[] = {2, 4, 5, 9, 15, 50, 61, 80, 94, 102};
		int B[] = {3, 4, 5, 6, 9, 15, 25, 50, 61};

		ArrayList<Integer> res = DiffTri(A, B);
		System.out.println("- les valeurs contenues UNIQUEMENT dans A et UNIQUEMENT dans B sont : ");
		System.out.println(res);
	}
}
