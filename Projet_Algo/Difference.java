import java.util.Scanner;
import java.util.ArrayList;

public class Difference {
	
	public static ArrayList<Integer> Diff (int[] A, int[] B) {
		return Diff_process(A, B, 0, 0);
	}

	public static ArrayList<Integer> Diff_process (int[] A, int[] B, int ia, int ib) {
		ArrayList<Integer> tmp = new ArrayList<Integer>();
		
		if (ia < A.length) {
			if (Xsearch(A[ia], B) == false) {
				tmp.add(A[ia]);
			}
			tmp.addAll(Diff_process(A, B, ia+1, ib));
			return tmp;
		}

		if (ib < B.length) {
			if (Xsearch(B[ib], A) == false) {
				tmp.add(B[ib]);
			}
			tmp.addAll(Diff_process(A, B, ia, ib+1));
			if (ib != 0) { return tmp; }
		}
		return tmp;
	}

	public static boolean Xsearch (int findX, int A[]) {
		return Xsearch_process(findX, A, 0);
	}

	public static boolean Xsearch_process (int findX, int A[], int i) {
		if (i >= A.length) return false;
		if (A[i] == findX) return true;
		return Xsearch_process(findX, A, i+1);
	}


	public static void main (String[] args) {
		
		int A[] = {2, 9, 5, 10, 4, 7, 33, 25, 26, 15};
		int B[] = {3, 41, 2, 26, 33, 25, 26, 15, 200};
		
		ArrayList<Integer> res = Diff(A, B);
		System.out.println("- les valeurs contenues UNIQUEMENT dans A et UNIQUEMENT dans B sont : ");
		System.out.println(res);
	}
}
