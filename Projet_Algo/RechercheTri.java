import java.util.Scanner;

public class RechercheTri {
	
	public static boolean XsearchTri(int findX, int A[]) {
		return XsearchTri_process(findX, A, 0, A.length-1);
	}

	public static boolean XsearchTri_process (int findX, int A[], int i, int j) {
		int k = (i + j) /2;		
		if (findX == A[k]) return true;
		if (i >= j) return false;
		if (findX < A[k]) {
			return XsearchTri_process(findX, A, i, k-1);
		}
		return XsearchTri_process(findX, A, k+1, j);
	}

	public static void main (String[] args) {
		
		int A[] = {2, 4, 5, 9, 15, 50, 61, 80, 94, 102};

		Scanner reader = new Scanner (System.in);
		System.out.print("- Entrer la valeur rechercher : ");
		int findX = reader.nextInt();

		boolean X = XsearchTri(findX, A);
		System.out.println(X);
	}
}
