import java.util.Scanner;
import java.util.ArrayList;

public class UnionTri {
	
	public static ArrayList<Integer> UnionTri (int[] A, int[] B) {
		return UnionTri_process(A, B, 0, 0);
	}

	public static ArrayList<Integer> UnionTri_process (int[] A, int[] B, int ia, int ib) {
		ArrayList<Integer> save = new ArrayList<Integer>();
		
		if (ia < A.length) {
			save.add(A[ia]);
			save.addAll(UnionTri_process(A, B, ia+1, ib));
			return save;
		}

		if (ib < B.length) {
			if (XsearchTri(B[ib], A) == false) {
				save.add(B[ib]);
			}
			save.addAll(UnionTri_process(A, B, ia, ib+1));
		}
		return save;
	}

	public static boolean XsearchTri (int findX, int A[]) {
		return XsearchTri_process(findX, A, 0, A.length-1);
	}

	public static boolean XsearchTri_process (int findX, int A[], int i, int j) {
		int k = (i + j) /2;		
		if (findX == A[k]) return true;
		if (i >= j) return false;
		if (findX < A[k]) {
			return XsearchTri_process(findX, A, i, k-1);
		}
		else {
			return XsearchTri_process(findX, A, k+1, j);
		}
	}

	public static void main (String[] args) {
		
		int A[] = {2, 4, 5, 9, 15, 50, 61, 80, 94, 102};
		int B[] = {3, 4, 5, 6, 9, 15, 25, 50, 61};

		ArrayList<Integer> lol = UnionTri(A, B);
		System.out.println("- TOUTES les valeurs contenues dans A et B sont : ");
		System.out.println(lol);
	}
}
