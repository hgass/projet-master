import java.util.Scanner;
import java.util.ArrayList;

public class IntersectionTri {
	
	public static ArrayList<Integer> InterTri (int[] A, int[] B) {
		return InterTri_process(A, B, 0);
	}

	public static ArrayList<Integer> InterTri_process (int[] A, int[] B, int ib) {
		ArrayList<Integer> tmp = new ArrayList<Integer>();

		if (ib < B.length) {
			tmp.addAll(InterTri_process(A, B, ib+1));
			if (XsearchTri(B[ib], A) != false && tmp.contains(B[ib]) == false) {
				tmp.add(B[ib]);
			}
		}
		return tmp;
	}

	public static boolean XsearchTri (int findX, int A[]) {
		return XsearchTri_process(findX, A, 0, A.length-1);
	}

	public static  boolean XsearchTri_process (int findX, int A[], int i, int j) {
		int k = (i + j) /2;	
		if (findX == A[k]) return true;
		if (i >= j) return false;
		if (findX < A[k]) {
			return XsearchTri_process(findX, A, i, k-1);
		}
		else {
			return XsearchTri_process(findX, A, k+1, j);
		}
	}

	public static void main (String[] args) {
		
		int A[] = {2, 4, 5, 9, 15, 50, 61, 80, 94, 102};
		int B[] = {3, 4, 5, 6, 9, 15, 25, 50, 61};

		ArrayList<Integer> res = InterTri(A, B);
		System.out.println("- les valeurs contenues dans A ET dans B sont : ");
		System.out.println(res);
	}
}
