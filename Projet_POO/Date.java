////////////////////////
// GASS Helene M1BSBI //
////////////////////////


//Ajout des methodes getJour(), getMois(), getAnnee()
//Ajout de la methode nbAnnee() qui permet de calculer l'age en fonction de la date exacte, donc en tenant compte de l'annee mais aussi mois et du jour
//Ajout de la methode toDate()

import java.util.Vector;
import java.util.StringTokenizer;

public class Date {

//// Attributs
	private int jour;
	private int mois;
	private int annee;
	static int jourMax [] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

//// Constructeur
	public Date(int jour, int mois, int annee) {
		setMois(mois);
		setJour(jour);
		setAnnee(annee);
	}

/// Getteur et Setteur
	public int getJour() {
		return this.jour;
	}

	private void setJour(int jour) {
		this.jour = jour;
		if (this.jour >= jourMax[this.mois]) {
			this.jour = jourMax[this.mois];
		}
		else if (this.jour <=0) {
			this.jour = 1;
		}
	}

	public int getMois() {
		return this.mois;
	}

	private void setMois(int mois) {
		this.mois = mois;
		if (this.mois >= jourMax.length - 1) {
			this.mois = jourMax.length - 1;
		}
		else if (this.mois <= 0) {
			this.mois = 1;
		}		
	}

	public int getAnnee() {
		return this.annee;
	}

	private void setAnnee(int annee) {
		this.annee = annee;
	}

//// Méthodes
	public void AjouteJour(int nbJour) {
		this.jour += nbJour;
		int temporaire = 0;
		while (this.jour > jourMax[mois]) {
			temporaire = this.jour - jourMax[mois];
			this.mois++;
			this.jour = temporaire;
			if (this.mois > jourMax.length - 1) {
				mois -= jourMax.length - 1;
				this.annee++;
			}
		}	
	}

	public void AfficheDate() {
		System.out.println("Nous sommes le " + this.jour + "/" + this.mois + "/" + this.annee);
	}

	public static int nbAnnee(Date currentDate, Date compareDate) {
		int diffYear = currentDate.getAnnee() - compareDate.getAnnee();
		
		if (currentDate.getMois() < compareDate.getMois()) {
			diffYear -= 1;
		}
		else if (currentDate.getMois() == compareDate.getMois() &&
			 currentDate.getJour() < compareDate.getJour()) {
			diffYear -= 1;
		}
		return diffYear;
	}

	public static Date toDate(String s) {
		StringTokenizer date = new StringTokenizer(s, "/"); // delimiteur = '/'
		Vector<String> strDate = new Vector<String>();
		Vector<Integer> argDate = new Vector<Integer>();
		while (date.hasMoreTokens()) { //tant qu'il y a un mot
			String tmp = date.nextToken(); //prend le mot
			strDate.addElement(tmp); //sauvegarde un element dans le Vecteur
		}
		for (int i = 0; i < strDate.size(); i++) { //transformation de String en Int
			argDate.add(i, Integer.parseInt(strDate.get(i)));
		}
		if (argDate.size() < 3) {
			 System.out.println("erreur format date");
		}
		else {
			Date finalDate = new Date(argDate.get(0), argDate.get(1), argDate.get(2));
			return finalDate;		
		}
		return new Date(0, 0, 0);
	}

	public String dateToString() {
		return (this.jour + "/" + this.mois + "/" + this.annee);
	}
}
