////////////////////////
// GASS Helene M1BSBI //
////////////////////////


//fichier avec le 'main'

import java.util.Scanner;
import java.util.Vector;

public class ListeDePersonne {

	public static void main(String[] args) {
		Vector<Personne> allPersonne = lirePersonne();
		System.out.println("----- all personne -----");
		for (int i = 0; i < allPersonne.size(); i++) {
			Personne tmp = 	allPersonne.get(i);
			System.out.println(tmp.personneToString());
		}

		Scanner scanMonth = new Scanner(System.in);
		System.out.println("Choissir mois");
		int mois = scanMonth.nextInt();
		System.out.println("----- personne avec anniversaire durant le mois " + mois + " -----");
		Vector<Personne> annivThisMonth = extraire(allPersonne, mois);
		for (int i = 0; i < annivThisMonth.size(); i++) {
			Personne tmp = 	annivThisMonth.get(i);
			System.out.println(tmp.personneToString());
		}
	}

	public static Vector<Personne> lirePersonne() {
		Scanner entree = new Scanner(System.in);
		entree.useDelimiter("\n");
		System.out.println ("Nombre de personnes à entrer dans la liste");
		int nbPersonne = entree.nextInt();
		System.out.println ("Ecrire " + nbPersonne + " lignes formater: Nom Premon jj/mm/aaaa");
		Vector<Personne> listePersonne = new Vector<Personne>();
		String ligne = "";		
		for (int i = 1; i <= nbPersonne; i++) {
			ligne = entree.next(); //demande de rentrer une nouvelle ligne (nouvelle personne)
			listePersonne.addElement(Personne.toPersonne(ligne)); //sauvegarde les donnees
		}
		return listePersonne;
	}

	public static Vector<Personne> extraire(Vector<Personne> v, int mois) {
		Vector<Personne> personneAnniv = new Vector<Personne>();
		Personne tmp;
		for (int i = 0; i < v.size(); i++) { //pour parcourir toutes les personnes
			tmp = v.get(i);
			if (tmp.aSonAnniversaire(mois) == true) {
				personneAnniv.addElement(tmp); //sauvegarde un element dans le Vecteur
			}
		}
		return personneAnniv;
	}
}
