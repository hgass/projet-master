////////////////////////
// GASS Helene M1BSBI //
////////////////////////


//Ajout de la methode toPersonne()

import java.util.Calendar;
import java.util.Vector;
import java.util.StringTokenizer;

public class Personne {

//// Attributs
	private String nom;
	private String prenom;
	private Date dateNaissance;

//// Constructeur
	public Personne(String n, String p, Date d) {
		this.nom = n;
		this.prenom = p;
		this.dateNaissance = d;
	}

//// Getteur
	public String getNom() {
		return this.nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public Date getDateNaissance(){
		return this.dateNaissance;
	}

	public int getJourNaissance() {
		return this.dateNaissance.getJour();
	}

	public int getMoisNaissance() {
		return this.dateNaissance.getMois();
	}

	public int getAnneeNaissance() {
		return this.dateNaissance.getAnnee();
	}


//// Methode
/*utilistation de Calendar qui nous permet d'avoir la date actuelle, et ainsi dans cette methode on calcul l'age excate en tenant compte de l'annee mais aussi du mois et du jour en utilisant la methode creer dans Date*/
	public int age() {
		Calendar cal = Calendar.getInstance();
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		
		Date current = new Date(day, month, year);
		int age = Date.nbAnnee(current, this.dateNaissance);
		return age;
	}

	public int age(Date d) {
		int age = Date.nbAnnee(d, this.dateNaissance);
		return age;
	}

	public boolean aSonAnniversaire(Date d) {		
		if (d.getJour() == this.dateNaissance.getJour() &&
		    d.getMois() == this.dateNaissance.getMois()) {
			return true;
		}
		return false;
	}

	public boolean aSonAnniversaire(int mois) {
		if (mois == this.dateNaissance.getMois()) {
			return true;
		}
		return false;
	}

	public static Personne toPersonne(String s) {
		StringTokenizer str = new StringTokenizer(s); //delimiteur par default = espace
		Vector<String> strPersonne = new Vector<String>();
		String tmp;
		while (str.hasMoreTokens()) { //tant qu'il y a un mot
			tmp = str.nextToken(); //prend le mot
			strPersonne.addElement(tmp); //sauvegarde le mot dans le Vecteur
		}
		if (strPersonne.size() < 3) {
			System.out.println("Error: format");
		}
		else {
			Personne finalPers = new Personne(strPersonne.get(0), strPersonne.get(1), Date.toDate(strPersonne.get(2)));
//appele de 'toDate' car la date est le 3eme element du Vecteur. On fait appele a cette methode car c'est elle qui decoupe cette partie et nous renvoye un objet Date
			return finalPers;
		}
		return new Personne("Error", "Error", new Date(0, 0, 0));
	}

	public String personneToString() {
		return ("Nom : " + this.nom + ",  Pernom: " + this.prenom + ",  Anniversaire: " + this.dateNaissance.dateToString());
	}
}
