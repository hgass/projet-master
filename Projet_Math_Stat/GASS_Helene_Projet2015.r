#PROJET R
#GASS Hélène - M2 BSBI

#Donner exercice 2 du CC2
X  <- c(2,4,1,4,1,3,2,2,4,4)
mu <- c(0.25,0.25,0.5)
PI <- matrix(c(0.6,0.3,0.1,
               0.4,0.1,0.5,
               0.3,0.5,0.2), ncol=3, byrow=T)
P  <- matrix(c(0.2,0.3,0.2,0.3,
               0.1,0.3,0.4,0.2,
               0.3,0.2,0.4,0.1), ncol=4, byrow=T)

# fonction baum_welch(X, mu, PI, P)
baum_welch <- function(X, mu, PI, P) {
    q <- length(mu)
    n <- length(X)
    FVec <- c()

    # pour n=1 on a F1(q) = mu[q] * P[q,X[n]]
    for (j in 1:q) {
        FVec <- append(FVec, mu[j] * P[j, X[1]])
    }

    F <- matrix(FVec, ncol=q, byrow=T)

    # pour n > 1 on a Fn(q) = p[q,X[n]] * sum(PI[l,q] * Fn-1(q))
    # l = ligne de PI
    for (k in 2:n) { # passage de F1 a F2 ... Fn
        FVec <- c()
        for (i in 1:q) { # passage de Fn(1) a Fn(2) ... Fn(q)
            s <- 0
            for (j in 1:q) { # iteration de la ligne de la matrice PI * Fn-1
                s <- s + (PI[j,i] * F[k-1,j])
            }
            FVec <- append(FVec, P[i,X[k]] * s)
        }
        F <- rbind(F, FVec)
    }

    # on affiche les valeurs
    x <- 1
    for (k in 1:n) {
        for (j in 1:q) {
            cat(sprintf("F%s(%s) = %s\n", k, j, F[k,j]))
            x <- x + 1
        }
    }
}

# pour lancer la fonction taper --> baum_welch(X, mu, PI, P)
# avec les parametres correspondant --> si a=X, b=mu, c=PI, d=P, taper baum_welch(a, b, c, d) 
