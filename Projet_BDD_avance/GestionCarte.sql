--Créez un package pour la gestion de la carte :

SET SERVEROUTPUT ON
CREATE OR REPLACE PACKAGE GestionCarte IS
	PROCEDURE AffichePizza(namePizza pizza.nompiz%TYPE);
	PROCEDURE PizzaSansIng(produitCom ingredient.libelle%TYPE);
	PROCEDURE PizzaSansCat(categorieP categorie_ing.libellecat%TYPE);
	PROCEDURE AfficheMenu;
	PROCEDURE ModifTarif(piz tarif.numpiz%TYPE, tailles tarif.taille%TYPE, montant NUMBER);
	--TRIGGER HistoTarifs;
END GestionCarte;
/


CREATE OR REPLACE PACKAGE BODY GestionCarte IS

--a) un sous-programme AffichePizza(pizza) qui affiche les informations concernant une pizza (ingrédients, tarifs en fonction des tailles ordonnés par taille)

	PROCEDURE AffichePizza(namePizza IN pizza.nompiz%TYPE) 
	AS 
		CURSOR getIngredient(name pizza.nompiz%TYPE) IS 
			SELECT i.libelle
			FROM ingredient i JOIN composition c ON i.numing = c.ing JOIN pizza p ON c.pizza = p.numpiz
			WHERE p.nompiz = name;
		recIng getIngredient%ROWTYPE; 
	
		CURSOR getTailleAndPrix(name pizza.nompiz%TYPE) IS
			SELECT t.taille, t.prix 
			FROM tarif t JOIN pizza p ON p.numpiz = t.numpiz 
			WHERE p.nompiz = name 
			ORDER BY t.taille ASC; 
		recTailleAndPrix getTailleAndPrix%ROWTYPE; 
BEGIN 
	DBMS_OUTPUT.PUT_LINE('Pizza ' || namePizza); 

	DBMS_OUTPUT.PUT_LINE('|-- Ingredients :');
--	FOR recIng 
--	IN getIngredient(namePizza) 
	OPEN getIngredient (namePizza);--
		LOOP
			FETCH getIngredient INTO recIng;--
			EXIT WHEN getIngredient%NOTFOUND;--
			DBMS_OUTPUT.PUT_LINE('| |- ' || recIng.libelle); 
		END LOOP; 
	CLOSE getIngredient;--

	DBMS_OUTPUT.PUT_LINE('|- Taille et Prix :'); 
	FOR recTailleAndPrix 
	IN getTailleAndPrix(namePizza) 
		LOOP 
			DBMS_OUTPUT.PUT_LINE('| |- ' || recTailleAndPrix.taille || ': ' || recTailleAndPrix.prix ||'euros'); 
		END LOOP;
	END;


--b) un sous-programme PizzaSansIng(ing) qui affiche les pizzas qui ne contiennent pas lingrédient passé en paramètre

	PROCEDURE PizzaSansIng(produitCom ingredient.libelle%TYPE) 
	AS
		CURSOR c1 (produit ingredient.libelle%TYPE) IS
			SELECT p.nompiz
			FROM pizza p
			WHERE p.numpiz NOT IN (SELECT c.pizza
				            FROM composition c JOIN ingredient i ON c.ing = i.numing
				            WHERE i.libelle = produit);
		rec c1%ROWTYPE;
BEGIN	
	OPEN c1 (produitCom);
		DBMS_OUTPUT.PUT_LINE('Lingredient ' || produitCom || ' nest pas dans les pizzas :');
		LOOP
			FETCH c1 INTO rec;
			EXIT WHEN c1%NOTFOUND;
			DBMS_OUTPUT.PUT_LINE('- ' || rec.nompiz);
		END LOOP;
	CLOSE c1;
	END;


--c) un sous-programme PizzaSansCat(cat) qui affiche les pizzas qui ne contiennent pas dingrédient de la catégorie passée en paramètre

	PROCEDURE PizzaSansCat(categorieP categorie_ing.libellecat%TYPE)
	AS
		CURSOR c1 (cat categorie_ing.libellecat%TYPE) IS
			SELECT p.nompiz
			FROM pizza p
			WHERE p.numpiz NOT IN (SELECT c.pizza
						FROM categorie_ing a JOIN ingredient i ON a.numcat = i.categorie
						JOIN composition c ON i.numing = c.ing
						WHERE a.libellecat = cat);
		rec c1%ROWTYPE;
	BEGIN
	OPEN c1 (categorieP);
		DBMS_OUTPUT.PUT_LINE('Les ingredients de la categorie ' || categorieP || ' ne sont pas dans les pizzas :');
		LOOP
			FETCH c1 INTO rec;
			EXIT WHEN c1%NOTFOUND;
			DBMS_OUTPUT.PUT_LINE('- ' || rec.nompiz);
		END LOOP;
	CLOSE c1;
	END;


--d) un sous-programme AfficheMenu qui affiche les noms de toutes les pizzas avec leurs ingrédients entre parenthèses

	PROCEDURE AfficheMenu 
	IS
		CURSOR c1 IS
			SELECT p.nompiz, wmsys.wm_concat(i.libelle) libelle
			FROM composition c
			JOIN ingredient i ON i.numing = c.ing 
			JOIN pizza p ON p.numpiz = c.pizza
			GROUP BY p.nompiz;
 			rec_lig c1%ROWTYPE;
	BEGIN
	OPEN c1;
		LOOP
			FETCH c1 INTO rec_lig;
			EXIT WHEN c1%NOTFOUND;
			DBMS_OUTPUT.PUT_LINE('la pizza ' || rec_lig.nompiz || ' : (' ||rec_lig.libelle || ')');
		END LOOP;
	CLOSE c1;
	END;


--e) un sous-programme ModifTarif(numpiz, taille, montant) qui permet de modifier le prix dune pizza en sassurant de la cohérence du nouveau prix (le prix reste positif et le prix est supérieur(inférieur) au prix dune pizza du même type et de taille inférieure (supérieur))

	PROCEDURE ModifTarif(piz tarif.numpiz%TYPE, tailles tarif.taille%TYPE, montant NUMBER) AS

		CURSOR c1(no tarif.numpiz%TYPE, t tarif.taille%TYPE) IS
		    SELECT * FROM (
        		SELECT numpiz, prix, taille,
		    	    lead(prix,1,0) over(order by taille) as nextPrix,
		    	    lag(prix,1,0) over(order by taille) as prevPrix
		    	    FROM tarif
		    	    WHERE numpiz = no
		    	    ORDER BY numpiz
		    	)
		    WHERE taille = t;
		recC1 c1%ROWTYPE;
		nouvprix tarif.prix%TYPE;
	BEGIN

  		OPEN c1 (piz, tailles);
    		FETCH c1 INTO recC1;
    		IF c1%FOUND THEN
        		nouvprix := recC1.prix + montant;
        		IF (nouvprix < recC1.nextPrix OR recC1.nextPrix = 0)
        		    AND (nouvprix > recC1.prevPrix)
        		    AND (nouvprix > 0)
        		THEN
        		    	DBMS_OUTPUT.PUT_LINE('la pizza ' || piz || ' et de taille ' || tailles || ' a pour nouveau prix ' || nouvprix);
			        	UPDATE tarif
			        	SET prix = nouvprix
			        	WHERE piz = numpiz
        		        	AND taille = tailles;
        		ELSE
        			DBMS_OUTPUT.PUT_LINE('Le montant a ajouter ne donne pas un prix final adapte par rapport aux caracteristiques (prix et taille) des autres pizzas');
			END IF;
    		ELSE
    			DBMS_OUTPUT.PUT_LINE('Pizza inexistante');
    		END IF;
  		CLOSE c1;
	END;	

END GestionCarte;
/


--f) un trigger HistoTarifs qui alimente une table historique_tarifs(numt, numpiz, taille, prix, date_deb_tarif, date_fin_tarif) en cas de modification de tarif

CREATE TABLE historique_tarifs (
	numt            NUMBER(11) NOT NULL,
	numpiz          NUMBER(2),
	taille          NUMBER(2),
	prix            NUMBER(2),
	date_deb_tarif  DATE,
	date_fin_tarif  DATE,
	constraint pkhistorique_tarifs primary key (numt),
	FOREIGN KEY (numpiz) REFERENCES pizza (numpiz)
);

CREATE SEQUENCE seq_histo_tarif_id START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER HistoTarifs
	BEFORE UPDATE ON tarif
	FOR EACH ROW
DECLARE
	v_current_seq NUMBER;
BEGIN
	SELECT seq_histo_tarif_id.nextval INTO v_current_seq FROM dual;
	INSERT INTO historique_tarifs
	(numt, numpiz, taille, prix, date_deb_tarif, date_fin_tarif)
    	VALUES (v_current_seq, :old.numpiz, :old.taille, :old.prix, :old.datet, SYSDATE);
END;
/
