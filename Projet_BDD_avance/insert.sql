INSERT INTO categorie_ing VALUES (1, 'fruits et legumes');
INSERT INTO categorie_ing VALUES (2, 'viande');
INSERT INTO categorie_ing VALUES (3, 'laitier');
INSERT INTO categorie_ing VALUES (4, 'epices');
INSERT INTO categorie_ing VALUES (5, 'poisson');
INSERT INTO categorie_ing VALUES (6, 'autres');


INSERT INTO ingredient VALUES (1, 'sauce tomate', 1);
INSERT INTO ingredient VALUES (2, 'tomate fraiche', 1);
INSERT INTO ingredient VALUES (3, 'mozzarella', 3);
INSERT INTO ingredient VALUES (4, 'jambon', 2);
INSERT INTO ingredient VALUES (5, 'chorizo', 2);
INSERT INTO ingredient VALUES (6, 'poivron', 1);
INSERT INTO ingredient VALUES (7, 'creme fraiche', 3);
INSERT INTO ingredient VALUES (8, 'poivre', 4);
INSERT INTO ingredient VALUES (9, 'gruyere', 3);
INSERT INTO ingredient VALUES (10, 'saumon', 5);
INSERT INTO ingredient VALUES (11, 'ananas', 1);
INSERT INTO ingredient VALUES (12, 'piment', 4);
INSERT INTO ingredient VALUES (13, 'champignon', 6);


INSERT INTO pizza VALUES (1, 'Reine');
INSERT INTO pizza VALUES (2, 'Parma');
INSERT INTO pizza VALUES (3, 'Forestière');
INSERT INTO pizza VALUES (4, 'Hawaiienne');
INSERT INTO pizza VALUES (5, 'Mexicaine');
INSERT INTO pizza VALUES (6, 'Marinera');
INSERT INTO pizza VALUES (7, 'Diavola');
INSERT INTO pizza VALUES (8, 'Supreme');


INSERT INTO composition VALUES (1, 1);
INSERT INTO composition VALUES (1, 3);
INSERT INTO composition VALUES (2, 1);
INSERT INTO composition VALUES (2, 4);
INSERT INTO composition VALUES (3, 1);
INSERT INTO composition VALUES (3, 13);
INSERT INTO composition VALUES (4, 1);
INSERT INTO composition VALUES (4, 4);
INSERT INTO composition VALUES (4, 11);
INSERT INTO composition VALUES (5, 1);
INSERT INTO composition VALUES (5, 4);
INSERT INTO composition VALUES (5, 5);
INSERT INTO composition VALUES (6, 7);
INSERT INTO composition VALUES (6, 10);
INSERT INTO composition VALUES (7, 1);
INSERT INTO composition VALUES (7, 5);
INSERT INTO composition VALUES (7, 12);
INSERT INTO composition VALUES (8, 1);
INSERT INTO composition VALUES (8, 4);
INSERT INTO composition VALUES (8, 6);
INSERT INTO composition VALUES (8, 9);
INSERT INTO composition VALUES (8, 13);


INSERT INTO tarif VALUES (1, 2, TO_DATE('10/04/2014','dd/mm/yyyy'), 10);
INSERT INTO tarif VALUES (1, 4, TO_DATE('10/04/2014','dd/mm/yyyy'), 12);
INSERT INTO tarif VALUES (1, 8, TO_DATE('10/04/2014','dd/mm/yyyy'), 14);
INSERT INTO tarif VALUES (2, 2, TO_DATE('23/05/2014','dd/mm/yyyy'), 11);
INSERT INTO tarif VALUES (2, 4, TO_DATE('23/05/2014','dd/mm/yyyy'), 12);
INSERT INTO tarif VALUES (2, 8, TO_DATE('23/05/2014','dd/mm/yyyy'), 14);
INSERT INTO tarif VALUES (3, 2, TO_DATE('28/10/2014','dd/mm/yyyy'), 10.50);
INSERT INTO tarif VALUES (3, 4, TO_DATE('28/10/2014','dd/mm/yyyy'), 13);
INSERT INTO tarif VALUES (3, 8, TO_DATE('28/10/2014','dd/mm/yyyy'), 15);
INSERT INTO tarif VALUES (4, 2, TO_DATE('21/12/2014','dd/mm/yyyy'), 10);
INSERT INTO tarif VALUES (4, 4, TO_DATE('21/12/2014','dd/mm/yyyy'), 12);
INSERT INTO tarif VALUES (4, 8, TO_DATE('21/12/2014','dd/mm/yyyy'), 15);
INSERT INTO tarif VALUES (5, 2, TO_DATE('29/12/2014','dd/mm/yyyy'), 10);
INSERT INTO tarif VALUES (5, 4, TO_DATE('29/12/2014','dd/mm/yyyy'), 11.50);
INSERT INTO tarif VALUES (5, 8, TO_DATE('29/12/2014','dd/mm/yyyy'), 14);
INSERT INTO tarif VALUES (6, 2, TO_DATE('07/01/2015','dd/mm/yyyy'), 13);
INSERT INTO tarif VALUES (6, 4, TO_DATE('07/01/2015','dd/mm/yyyy'), 14.50);
INSERT INTO tarif VALUES (6, 8, TO_DATE('07/01/2015','dd/mm/yyyy'), 16);
INSERT INTO tarif VALUES (7, 2, TO_DATE('15/02/2015','dd/mm/yyyy'), 12);
INSERT INTO tarif VALUES (7, 4, TO_DATE('15/02/2015','dd/mm/yyyy'), 13);
INSERT INTO tarif VALUES (7, 8, TO_DATE('15/02/2015','dd/mm/yyyy'), 14);
INSERT INTO tarif VALUES (8, 2, TO_DATE('24/03/2015','dd/mm/yyyy'), 11.50);
INSERT INTO tarif VALUES (8, 4, TO_DATE('24/03/2015','dd/mm/yyyy'), 13);
INSERT INTO tarif VALUES (8, 8, TO_DATE('24/03/2015','dd/mm/yyyy'), 14.50);


INSERT INTO livreur VALUES (1, 'Antoine', TO_DATE('10/04/2014','dd/mm/yyyy'), TO_DATE('16/02/2015','dd/mm/yyyy'), '0615432898');
INSERT INTO livreur VALUES (2, 'Remi', TO_DATE('23/05/2014','dd/mm/yyyy'), NULL, '0687654321');
INSERT INTO livreur VALUES (3, 'Sebastien', TO_DATE('28/10/2014','dd/mm/yyyy'), NULL, '0645326708');
INSERT INTO livreur VALUES (4, 'Luc', TO_DATE('11/12/2014','dd/mm/yyyy'), NULL, '0631257687');
INSERT INTO livreur VALUES (5, 'Julien', TO_DATE('09/01/2015','dd/mm/yyyy'), NULL, '0616875457');
INSERT INTO livreur VALUES (6, 'Romeo', TO_DATE('08/02/2015','dd/mm/yyyy'), TO_DATE('14/03/2015','dd/mm/yyyy'), '0635498765');
INSERT INTO livreur VALUES (7, 'Frank', TO_DATE('15/03/2015','dd/mm/yyyy'), TO_DATE('01/04/2015','dd/mm/yyyy'), '0698749875');


INSERT INTO commande VALUES (1, 'Martin', 'Nicolas', '0615432896', 'Rue des jonquilles', 'Residence Le Jardin', 67000, 'Strasbourg',
TO_DATE('20/10/2015, 11:00','dd/mm/yyyy, HH24:MI'), TO_DATE('20/10/2015, 11:00','dd/mm/yyyy, HH24:MI'), 2, 'livre');
INSERT INTO commande VALUES (2, 'Muller', 'Jean', '0615438898', 'Rue des lilas', NULL, 67400, 'Illkirch',
TO_DATE('20/10/2015, 12:30','dd/mm/yyyy, HH24:MI'), NULL, 3, 'livre');
INSERT INTO commande VALUES (3, 'Barreau', 'Lise', '0615412898', 'Route de Colmar', NULL, 67100, 'Strasbourg',
TO_DATE('28/10/2014, 20:00','dd/mm/yyyy, HH24:MI'), NULL, 2, NULL); 
INSERT INTO commande VALUES (4, 'Smith', 'John', '0698745633', 'Route du temps', NULL, 67000, 'Strasbourg',
TO_DATE('14/12/2014, 13:15','dd/mm/yyyy, HH24:MI'), NULL, 4, NULL);
INSERT INTO commande VALUES (5, 'Thomes', 'Luc', '0666658887', 'Route de Rome', 'Residence Paul Appel', 67000, 'Strasbourg',
TO_DATE('5/01/2015, 18:00','dd/mm/yyyy, HH24:MI'), TO_DATE('5/01/2015, 18:30','dd/mm/yyyy, HH24:MI'), 4, 'livre');
INSERT INTO commande VALUES (6, 'Loebs', 'Celine', '0606985632', 'Route de Finkwiller', NULL, 67000, 'Strasbourg',
TO_DATE('22/01/2015, 11:30','dd/mm/yyyy, HH24:MI'), TO_DATE('22/01/2015, 12:00','dd/mm/yyyy, HH24:MI'), 5, 'livre');
INSERT INTO commande VALUES (7, 'Lision', 'Aurore', '0606966632', 'Route de Menton', NULL, 06000, 'Menton',
TO_DATE('22/10/2015, 11:30','dd/mm/yyyy, HH24:MI'), TO_DATE('26/10/2015, 19:49','dd/mm/yyyy, HH24:MI'), 3, NULL);
INSERT INTO commande VALUES (8, 'Martin', 'Gérard', '0644444432', 'Route de bof', 'Residence Pourrie', 01110, 'BofLand',
TO_DATE('22/10/2015, 12:30','dd/mm/yyyy, HH24:MI'), TO_DATE('26/10/2015, 19:40','dd/mm/yyyy, HH24:MI'), 2, 'livre');
INSERT INTO commande VALUES (9, 'Vico', 'Louis', '0644558962', 'Route de Stras', 'Residence des fleurs', 48568, 'Bof',
TO_DATE('24/12/2015, 12:30','dd/mm/yyyy, HH24:MI'), TO_DATE('26/12/2015, 19:40','dd/mm/yyyy, HH24:MI'), 7, NULL);
INSERT INTO commande VALUES (10, 'Martin', 'Gérard', '0644444432', 'Route de bof', 'Residence Pourrie', 01110, 'BofLand',
TO_DATE('16/12/2015, 12:30','dd/mm/yyyy, HH24:MI'), TO_DATE('16/12/2015, 19:40','dd/mm/yyyy, HH24:MI'), 2, NULL);


INSERT INTO ligne_cmd VALUES (1, 1, 2, 2, 5);
INSERT INTO ligne_cmd VALUES (1, 2, 2, 1, 0);
INSERT INTO ligne_cmd VALUES (2, 2, 4, 1, 0);
INSERT INTO ligne_cmd VALUES (3, 3, 2, 2, 0);
INSERT INTO ligne_cmd VALUES (4, 4, 4, 2, 5);
INSERT INTO ligne_cmd VALUES (5, 5, 4, 2, 5);
INSERT INTO ligne_cmd VALUES (5, 8, 4, 1, 0);
INSERT INTO ligne_cmd VALUES (6, 4, 4, 1, 0);
INSERT INTO ligne_cmd VALUES (7, 4, 4, 2, 0);
INSERT INTO ligne_cmd VALUES (8, 4, 4, 5, 0);
INSERT INTO ligne_cmd VALUES (7, 2, 4, 5, 0);
INSERT INTO ligne_cmd VALUES (9, 6, 8, 1, 0);
INSERT INTO ligne_cmd VALUES (10, 5, 2, 5, 5);
INSERT INTO ligne_cmd VALUES (10, 7, 4, 1, 0);
