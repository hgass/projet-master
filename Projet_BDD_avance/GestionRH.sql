--Créez un package pour la gestion de la carte :

SET SERVEROUTPUT ON
CREATE OR REPLACE PACKAGE GestionRH IS
	PROCEDURE AfficheLivreurs;
	PROCEDURE NbLivraisonsLivreurMois(livreur commande.livreur%TYPE, mois NUMBER, annee NUMBER);
END GestionRH;
/


CREATE OR REPLACE PACKAGE BODY GestionRH IS

--a) un sous-programme AfficheLivreurs qui affiche le prénom des livreurs et leur ancienneté en nombre de mois depuis leur embauche

	PROCEDURE AfficheLivreurs 
	AS
		CURSOR c1 IS
			SELECT l.prenom, l.date_embauche
			FROM livreur l;
 		livreur_lig c1%ROWTYPE;
	BEGIN
		OPEN c1;
			LOOP	
				FETCH c1 INTO livreur_lig;
				EXIT WHEN c1%NOTFOUND;
				DBMS_OUTPUT.PUT_LINE('le livreur ' || livreur_lig.prenom || ' a commence a travailler il y a ' || FLOOR(MONTHS_BETWEEN(SYSDATE, livreur_lig.date_embauche)) || ' mois');
			END LOOP;
		CLOSE c1;
	END;


--b) un sous-programme NbLivraisonsLivreurMois(livreur, mois, annee) qui affiche le nombre de livraisons effectuées par un livreur passé en paramètre pour le mois donné de l'année donnée

	PROCEDURE NbLivraisonsLivreurMois(livreur commande.livreur%TYPE, mois NUMBER, annee NUMBER) 
	AS
		nbliv NUMBER;
	BEGIN
		SELECT COUNT(livreur) INTO nbliv
		FROM commande
		WHERE livreur = livreur AND mois = extract(month FROM dateheure_cmd) AND annee = extract(year FROM dateheure_cmd);
		DBMS_OUTPUT.PUT_LINE('Le livreur numero ' || livreur || ' a le ' || mois || '/' || annee || ' fait ' || nbliv || ' livraisons');
	END;

END GestionRH;
/
