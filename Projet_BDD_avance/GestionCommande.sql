-- Créer un package pour la Gestion des commandes : 

SET SERVEROUTPUT ON
CREATE OR REPLACE PACKAGE GestionCommande IS 
	PROCEDURE AfficheProchainesCommandes;
	FUNCTION CoutLigneCommande(numcom commande.numc%TYPE, Lpizza tarif.numpiz%TYPE, Ltaille tarif.taille%TYPE) RETURN NUMBER;
	FUNCTION CoutCommande(numcom commande.numc%TYPE) RETURN NUMBER;
	FUNCTION NbCommandesJour(datecom commande.dateheure_cmd%TYPE) RETURN NUMBER;
	FUNCTION NbPizzaCommandeJour(datecom commande.dateheure_cmd%TYPE) RETURN NUMBER;
	FUNCTION GainJour(dategain commande.dateheure_cmd%TYPE) RETURN NUMBER;
	FUNCTION GainMois(year CHAR, month CHAR) RETURN NUMBER;
	FUNCTION GainNDerniersMois(num NUMBER) RETURN NUMBER;
	PROCEDURE Facture(COM commande.numc%TYPE);
END GestionCommande;
/

CREATE OR REPLACE PACKAGE BODY GestionCommande IS 

--a) un sous-programme AfficheProchainesCommandes qui affiche les numéros des commandes à livrer dans la prochaine heure avec la quantité de pizzas commandées, la ville et l'adresse où livrer, par ordre d'heure de livraison croissant 	

	PROCEDURE AfficheProchainesCommandes 
	AS 
		CURSOR a1 IS 
			SELECT l.numc, TO_CHAR (c.dateheure_liv, 'dd/mm/yyyy, HH24:MI') "dateheure_liv", l.quantite, c.ville, c.adresse1, c.adresse2
			FROM commande c
			JOIN ligne_cmd l ON c.numc = l.numc 
			WHERE c.numc IN (
				SELECT numc
				FROM commande 
				WHERE (dateheure_liv > sysdate
					AND dateheure_liv < sysdate+1/24)
			)
			ORDER BY dateheure_liv;
	BEGIN
		FOR e IN a1 
		LOOP
			DBMS_OUTPUT.PUT_LINE('-Commande : '|| e.numc);
			DBMS_OUTPUT.PUT_LINE('| Quantite : '|| e.quantite);
			DBMS_OUTPUT.PUT_LINE('| Ville : '|| e.ville);
			DBMS_OUTPUT.PUT_LINE('| Adresse : '|| e.adresse1 ||', '|| e.adresse2);
		END LOOP;
	END;


--b) un sous-programme CoutLigneCommande(numcom, pizza, taille) qui retourne le coût d'une ligne de la commande (en tenant compte des éventuelles remises) 

	FUNCTION CoutLigneCommande(numcom commande.numc%TYPE, Lpizza tarif.numpiz%TYPE, Ltaille tarif.taille%TYPE)
	RETURN NUMBER 
	AS
		CoutLigne tarif.prix%TYPE;
		Remise ligne_cmd.remise%TYPE;
		Prix tarif.prix%TYPE;

	BEGIN

		SELECT t.prix INTO Prix
		FROM tarif t 
		WHERE t.numpiz = Lpizza  AND t.taille = Ltaille;

		SELECT l.quantite* Prix INTO CoutLigne
		FROM ligne_cmd l
		WHERE l.numc = numcom AND l.pizza = Lpizza AND l.taille = Ltaille;

		SELECT l.remise INTO Remise
		FROM ligne_cmd l 
		WHERE l.numc = numcom AND l.pizza = Lpizza AND l.taille = Ltaille;
	
		IF Remise != 0
			THEN 
			CoutLigne := CoutLigne * (1-Remise/100);
		END IF;

	RETURN CoutLigne;
	END;


--c) un sous-programme CoutCommande(numc) qui retourne le coût total d'une commande 

	FUNCTION CoutCommande(numcom commande.numc%TYPE)
	RETURN NUMBER 
	AS
		Total NUMBER := 0;
		Ligne tarif.prix%TYPE;

	-- Faire un fetch
		CURSOR C1 IS
			    SELECT l.pizza AS PIZ, l.taille AS TAI
			    FROM ligne_cmd l
			    WHERE l.numc = numcom;

	BEGIN

	    FOR n IN C1
	    LOOP
	            SELECT CoutLigneCommande(numcom, n.PIZ, n.TAI) INTO Ligne
	            FROM dual;
	            Total := Total + Ligne;
	    END LOOP;

	RETURN Total;
	END;


--d) un sous-programme NbCommandesJour(date) qui retourne le nombre de commandes reçues pour livraison le jour passé en paramètre

	FUNCTION NbCommandesJour(datecom commande.dateheure_cmd%TYPE)
	RETURN NUMBER 
	AS
		NombreCom NUMBER;

	BEGIN

		SELECT COUNT(c.numc) INTO NombreCom
		FROM commande c
		WHERE c.dateheure_cmd > datecom AND c.dateheure_cmd < datecom+1;
	
	RETURN NombreCom;
	END;


--e) un sous-programme NbPizzasCommandesJour(date) qui retourne le nombre de pizzas vendues (livrées) le jour passé en paramètre

	FUNCTION NbPizzaCommandeJour(datecom commande.dateheure_cmd%TYPE)
	RETURN NUMBER 
	AS
		NombrePiz NUMBER;

	BEGIN

		SELECT SUM(l.quantite) INTO NombrePiz
		FROM ligne_cmd l
		WHERE l.numc IN	(SELECT c.numc
						FROM commande c
						WHERE c.dateheure_cmd > datecom
						AND c.dateheure_cmd < datecom+1);

	RETURN NombrePiz;
	END;


--f) un sous-programme GainJour(date) qui retourne le montant gagné le jour passé en paramètre

	FUNCTION GainJour(dategain commande.dateheure_cmd%TYPE)
	RETURN NUMBER 
	AS
		CURSOR getNumC(dateG commande.dateheure_cmd%TYPE) IS
			SELECT c.numc
			FROM commande c
			WHERE c.dateheure_cmd >= dateG
				AND c.dateheure_cmd < dateG+1;

		GainJ NUMBER := 0;
		tmp NUMBER;

	BEGIN

		FOR n IN getNumC(dategain)
		LOOP
			SELECT CoutCommande(n.numc) INTO tmp
		    FROM dual;
			GainJ := GainJ + tmp;
		END LOOP;

	RETURN GainJ;
	END;


-- g) un sous-programme GainMois(annee, mois) qui retourne le montant gagné sur un mois donné

	FUNCTION GainMois(year CHAR, month CHAR)
	RETURN NUMBER 
	AS
		CURSOR getNumC(dateM CHAR) IS
			SELECT c.numc
			FROM commande c
			WHERE c.dateheure_cmd >= TO_DATE(dateM, 'YYYY-MON')
				AND c.dateheure_cmd < ADD_MONTHS(TO_DATE(dateM, 'YYYY-MON'), 1);

		GainM NUMBER := 0;
		tmp NUMBER;

	BEGIN

		FOR n IN getNumC(year || '-' || month)
		LOOP
			SELECT CoutCommande(n.numc) INTO tmp
		    FROM dual;
			GainM := GainM + tmp;
		END LOOP;

	RETURN GainM;
	END;


--h) un sous-programme GainNDerniersMois(n) qui affiche le montant gagné sur les n derniers mois

	FUNCTION GainNDerniersMois(num NUMBER)
	RETURN NUMBER 
	AS
		CURSOR getNumC(num NUMBER) IS
			SELECT c.numc
			FROM commande c
				WHERE c.dateheure_cmd > ADD_MONTHS(sysdate, -num);

		GainM NUMBER := 0;
		tmp NUMBER;

	BEGIN

		FOR n IN getNumC(num)
		LOOP
			SELECT CoutCommande(n.numc) INTO tmp
		    FROM dual;
			GainM := GainM + tmp;
		END LOOP;

	RETURN GainM;
	END;


--i) un sous-programme Facture(commande) qui affiche le détail d'une commande (le nom de la pizzeria, date de la commande, noms des pizzas et tailles, quantité commandée, prix à l'unité, total de la ligne de commande sans remise, total de la remise, total de la ligne avec remise, montant de la commande avec remise, montant total de la remise)

	PROCEDURE Facture(COM commande.numc%TYPE)
	AS
		DateCom commande.dateheure_cmd%TYPE;
		RemiseLigne tarif.prix%TYPE;
		TTLigneRemise tarif.prix%TYPE;
		Prix tarif.prix%TYPE;
		CoutLigne tarif.prix%TYPE;

		TTcmd NUMBER := 0;
		TTRemise NUMBER := 0;

		CURSOR C1 IS
		    SELECT p.numpiz, p.nompiz, t.taille, l.quantite, t.prix
		    FROM pizza p, tarif t, ligne_cmd l
		    WHERE l.numc = COM
		    	AND l.pizza = t.numpiz
		    	AND l.taille = t.taille
		    	AND t.numpiz = p.numpiz;
	
		info C1%ROWTYPE;

	BEGIN

		-- Date de la commande
		SELECT c.dateheure_cmd INTO DateCom
		FROM commande c
		WHERE c.numc = COM;

		-- En-tête de la facture
		DBMS_OUTPUT.PUT_LINE('-Pizzeria Pronto');
		DBMS_OUTPUT.PUT_LINE('| Date : ' || DateCom);

	-- Fetcher les lignes !
		FOR e IN C1
		LOOP

			-- Prix = prix de la pizza
		  	SELECT t.prix INTO Prix
			FROM tarif t
			   	WHERE t.numpiz = e.numpiz
					AND t.taille = e.taille;

			-- CoutLigne = montant de la ligne sans remise
			SELECT l.quantite* Prix INTO CoutLigne
			FROM ligne_cmd l
			WHERE l.numc = COM
				AND l.pizza = e.numpiz
				AND l.taille = e.taille;

			-- Total de la ligne avec la remise
			SELECT CoutLigneCommande(COM, e.numpiz, e.taille) INTO TTLigneRemise
			FROM dual;
			TTcmd := TTcmd + TTLigneRemise;

			-- Montant de la remise pour la ligne
			RemiseLigne := CoutLigne - TTLigneRemise;
			TTRemise := TTRemise + RemiseLigne;

			-- Ecriture de la ligne sur la facture
			DBMS_OUTPUT.PUT_LINE('| -Pizza : ' || e.nompiz);
			DBMS_OUTPUT.PUT_LINE('| | Taille : ' || e.taille);
			DBMS_OUTPUT.PUT_LINE('| | Qtt : ' || e.quantite);
			DBMS_OUTPUT.PUT_LINE('| | Prix par unite : ' || Prix || ' Euro');
			DBMS_OUTPUT.PUT_LINE('| | Montant : ' || CoutLigne || ' Euro');
			DBMS_OUTPUT.PUT_LINE('| | Remise : ' || RemiseLigne || ' Euro');
			DBMS_OUTPUT.PUT_LINE('| | Sous-total : ' || TTLigneRemise || ' Euro');
		END LOOP;
	-- Fin du Fetch

		-- Fin de la facture
		DBMS_OUTPUT.PUT_LINE('| Montant total avec remise :' || TTcmd || ' Euro');
		DBMS_OUTPUT.PUT_LINE('| Montant de la remise : ' || TTRemise || ' Euro');
	END;

END GestionCommande;
/
