CREATE TABLE categorie_ing 
(numcat NUMBER(2), 
libellecat VARCHAR(20), 
constraint pkcategorie_ing primary key (numcat) 
); 
 
CREATE TABLE ingredient 
(numing NUMBER(2), 
libelle VARCHAR(40), 
categorie NUMBER(2), 
constraint pkingredient primary key (numing), 
FOREIGN KEY (categorie) REFERENCES categorie_ing (numcat) 
); 
 
CREATE TABLE pizza 
(numpiz NUMBER(2), 
nompiz VARCHAR(40), 
constraint pkpizza primary key (numpiz) 
); 
 
CREATE TABLE composition 
(pizza NUMBER(2), 
ing NUMBER(2), 
constraint pkcomposition primary key (pizza, ing), 
FOREIGN KEY (pizza) REFERENCES pizza (numpiz), 
FOREIGN KEY (ing) REFERENCES ingredient (numing) 
); 
 
CREATE TABLE tarif 
(numpiz NUMBER(2), 
taille NUMBER(1), 
datet DATE, 
prix NUMBER(4,2), 
constraint pktarif primary key (numpiz, taille),
FOREIGN KEY (numpiz) REFERENCES pizza (numpiz), 
CONSTRAINT c_taille CHECK (taille IN ('2', '4', '8')) 
); 
 
CREATE TABLE livreur 
(numl NUMBER(3), 
prenom VARCHAR(20), 
date_embauche DATE NOT NULL, 
date_fin_contrat DATE, 
tel CHAR(10) NOT NULL, 
constraint pklivreur primary key (numl) 
); 
 
CREATE TABLE commande 
(numc NUMBER(8), 
nomclient VARCHAR(30) NOT NULL,  
prenomclient VARCHAR(20), 
tel CHAR(10) NOT NULL, 
adresse1 VARCHAR(120) NOT NULL,  
adresse2 VARCHAR(120), 
codepostal CHAR(5) NOT NULL, 
ville VARCHAR (30) NOT NULL, 
dateheure_cmd DATE NOT NULL, 
dateheure_liv DATE, 
livreur NUMBER(3), 
etat CHAR(5),
constraint pkcommande primary key (numc), 
FOREIGN KEY (livreur) REFERENCES livreur (numl), 
CONSTRAINT c_etat CHECK (lower(etat) = 'livre') 
); 
 
CREATE TABLE ligne_cmd 
(numc NUMBER(8), 
pizza NUMBER(2), 
taille NUMBER(1), 
quantite NUMBER(2) NOT NULL,  
remise NUMBER(4,2) NOT NULL, 
FOREIGN KEY (numc) REFERENCES commande (numc),
FOREIGN KEY (pizza, taille) REFERENCES tarif (numpiz, taille)
);

